package salariati.repository.mock;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import salariati.enumeration.DidacticFunction;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeMock implements EmployeeRepositoryInterface {

	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	
	public EmployeeMock() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();
		
		Employee Ionel   = new Employee("Ionel","Pacuraru", "1254567890876", DidacticFunction.ASISTENT, "500");
		Employee Mihai   = new Employee("Mihai","Dumitrescu", "1234567890876", DidacticFunction.LECTURER, "2560");
		Employee Ionela  = new Employee("Ionela","Ionescu", "1334567890876", DidacticFunction.LECTURER, "6500");
		Employee Mihaela = new Employee("Mihaela","Pacuraru", "1134567890876", DidacticFunction.ASISTENT, "3500");
		Employee Vasile  = new Employee("Vasile","Georgescu", "1134587890876", DidacticFunction.TEACHER,  "8000");
		Employee Marin   = new Employee("Marin","Puscas", "1534587890876", DidacticFunction.TEACHER,  "250");
		
		employeeList.add( Ionel );
		employeeList.add( Mihai );
		employeeList.add( Ionela );
		employeeList.add( Mihaela );
		employeeList.add( Vasile );
		employeeList.add( Marin );
	}
	
	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		for(int i=0;i<employeeList.size();i++) {
			if (employeeList.get(i).getCnp().equals(newEmployee.getCnp())) {
				employeeList.set(i, newEmployee);
			}
		}
		PrintWriter writer = null;

		for (Employee employee:employeeList){
			//addEmployee(employee);
		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		return employeeList;
	}

	@Override
	public List<Employee> getEmployeeByCriteria() {
		List<Employee> employeeList = getEmployeeList();
		employeeList.sort(Comparator.comparing(Employee::getCnp));
		employeeList.sort((o1, o2) -> {
			int cmp = o1.getSalary().compareTo(o2.getSalary());
			if (cmp == 0)
				cmp = o1.getAge().compareTo(o2.getAge());
			return cmp;
		});
		return employeeList;
	}

	@Override
	public Employee getEmployeeByCnp(Employee e) {
		List<Employee> employeesList = getEmployeeList();
		for(Employee employee:employeesList){
			if(employee.getCnp().equals(e.getCnp())){
				return employee;
			}
		}
		return null;
	}

}
