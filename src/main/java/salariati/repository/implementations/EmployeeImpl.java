package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {
	
	private final String employeeDBFile = "employees.txt";
	private EmployeeValidator employeeValidator = new EmployeeValidator();

	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.write(employee.toString());
				bw.newLine();
				bw.close();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		List<Employee> employeeList = getEmployeeList();
		for(int i=0;i<employeeList.size();i++) {
			if (employeeList.get(i).equals(oldEmployee)) {
				employeeList.set(i, newEmployee);
			}
		}
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(employeeDBFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		writer.print("");
		writer.close();
		for (Employee employee:employeeList){
			addEmployee(employee);
		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {
					employee = Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		return employeeList;
	}


	@Override
	public List<Employee> getEmployeeByCriteria() {
		List<Employee> employeeList = getEmployeeList();
		employeeList.sort(Comparator.comparing(Employee::getCnp));
		employeeList.sort((o1, o2) -> {
			int cmp = o1.getSalary().compareTo(o2.getSalary());
			if (cmp == 0)
				cmp = o1.getAge().compareTo(o2.getAge());
			return cmp;
		});
		return employeeList;
	}

	public Employee getEmployeeByCnp(Employee e) {
		List<Employee> employeeList = getEmployeeList();
		for(Employee employee:employeeList){
			if(employee.equals(e)){
				return employee;
			}
		}
		return null;
	}

}
