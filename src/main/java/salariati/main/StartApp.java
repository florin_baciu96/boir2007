package salariati.main;

import salariati.exception.EmployeeException;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

//functionalitati
//i.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//ii.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//iii.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).
public class StartApp {
	
	public static void main(String[] args) {
		
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
		EmployeeController employeeController = new EmployeeController(employeesRepository);
		
		System.out.println(employeeController.getEmployeesList());
		System.out.println("----------------------------------");
		System.out.println(employeeController.getEmployeeByCriteria());
		int option=0;
		while (option!=2) {
			System.out.println("1. Adaugare");
			System.out.println("2. Afisare");
			System.out.println("3. Iesire");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			try {
				try {
					option = Integer.parseInt(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
				switch (option) {
					case 1:
						try {
							System.out.println("Introduceti firstName:");
							String firstName = br.readLine();
							System.out.println("Introduceti lastName:");
							String lastName = br.readLine();
							System.out.println("Introduceti functia:");
							String functia = br.readLine();
							System.out.println("Introduceti CNP:");
							String cnp = br.readLine();
							System.out.println("Introduceti salariu:");
							String salariu = br.readLine();
							employeeController.addEmployee(new Employee(firstName,lastName,cnp,DidacticFunction.ASISTENT,salariu));
							break;
						} catch (NumberFormatException e) {
							System.out.println("Introduceti date corecte!");
							break;
						}
					case 2:
						for(Employee _employee : employeeController.getEmployeesList())
							System.out.println(_employee.toString());
						break;

					case 3:
					break;
					default:
						System.out.println("Introduceti o optiune valida!");
				}


			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				System.out.println("Introduceti o optiune valida!");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Introduceti o optiune valida!");
			}
		}
	}







}
