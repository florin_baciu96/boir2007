package salariati.repository.mock;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.*;

public class IntegrationBigBangTest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void addTest() {
        Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.addEmployee(newEmployee);
        assertEquals(7, employeeRepository.getEmployeeList().size());
        assertTrue(newEmployee.equals(employeeRepository.getEmployeeList().get(controller.getEmployeesList().size() - 1)));
    }

    @Test
    public void modifyTest() {
        Employee newEmployee = new Employee("Florin", "Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = null;
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.modifyEmployee(oldEmployee, newEmployee);
        assertEquals(null, employeeRepository.getEmployeeByCnp(newEmployee));
    }

    @Test
    public void sortTest() {
        List<Employee> employeeList=employeeRepository.getEmployeeByCriteria();
        assertEquals("Marin",employeeList.get(0).getFirstName());
        assertEquals("Mihai",employeeList.get(1).getFirstName());
    }

    @Test
    public void big_bang() {
        Employee oldEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "10000");
        assertTrue(employeeValidator.isValid(oldEmployee));
        controller.addEmployee(oldEmployee);
        assertEquals(7, employeeRepository.getEmployeeList().size());
        assertTrue(oldEmployee.equals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1)));

        Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "100");
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.modifyEmployee(oldEmployee, newEmployee);
        assertEquals("100", employeeRepository.getEmployeeByCnp(newEmployee).getSalary());

        List<Employee> employeeList=employeeRepository.getEmployeeByCriteria();
        assertEquals("Florin",employeeRepository.getEmployeeByCriteria().get(0).getFirstName());

    }



}