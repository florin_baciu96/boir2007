package salariati.repository.mock;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import static org.junit.Assert.*;

public class EmployeeModifyTest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        employeeValidator = new EmployeeValidator();
    }

    @Test
    public void testRepositoryMock() {
        assertFalse(employeeRepository.getEmployeeList().isEmpty());
        assertEquals(6, employeeRepository.getEmployeeList().size());
    }

    @Test
    public void test1() {
        Employee newEmployee = new Employee("Florin", "Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = null;
        assertTrue(employeeValidator.isValid(newEmployee));
        employeeRepository.modifyEmployee(oldEmployee, newEmployee);
        assertEquals(null, employeeRepository.getEmployeeByCnp(newEmployee));
    }


    @Test
    public void test2() {
        Employee newEmployee = new Employee("Florin", "Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = new Employee("Florin", "DDD", "1234543345321", DidacticFunction.TEACHER, "1400");
        employeeRepository.addEmployee(oldEmployee);
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee, newEmployee);
        assertEquals(null, employeeRepository.getEmployeeByCnp(newEmployee));
    }

    @Test
    public void test3() {
        Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = new Employee("Florin","DDD", "1234567891234", DidacticFunction.TEACHER, "1400");
        employeeRepository.addEmployee(oldEmployee);
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertEquals("Baciu", employeeRepository.getEmployeeByCnp(newEmployee).getLastName());
    }

    @Test
    public void test4() {
        Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = new Employee("Florin","DDD", "1234567891234", DidacticFunction.TEACHER, "1400");
        employeeRepository.addEmployee(oldEmployee);
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertEquals("Baciu", employeeRepository.getEmployeeByCnp(newEmployee).getLastName());
    }

    @Test
    public void test5() {
        Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = new Employee("Florin","DDD", "1234567891234", DidacticFunction.TEACHER, "1400");
        employeeRepository.addEmployee(oldEmployee);
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeValidator.isValid(oldEmployee));
        employeeRepository.modifyEmployee(oldEmployee,newEmployee);
        assertEquals("Baciu", employeeRepository.getEmployeeByCnp(newEmployee).getLastName());
    }


}