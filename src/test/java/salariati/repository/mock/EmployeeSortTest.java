package salariati.repository.mock;

import org.junit.Before;
import org.junit.Test;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

import java.util.List;

import static org.junit.Assert.*;

public class EmployeeSortTest {
    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        employeeValidator = new EmployeeValidator();
    }

    @Test
    public void testRepositoryMock() {
        assertFalse(employeeRepository.getEmployeeList().isEmpty());
        assertEquals(6, employeeRepository.getEmployeeList().size());
    }

    @Test
    public void test1() {
        List<Employee> employeeList=employeeRepository.getEmployeeByCriteria();
        assertEquals("Marin",employeeList.get(0).getFirstName());
        assertEquals("Mihai",employeeList.get(1).getFirstName());
    }

}