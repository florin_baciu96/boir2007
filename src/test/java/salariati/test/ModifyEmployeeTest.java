package salariati.test;

import static org.junit.Assert.*;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

public class ModifyEmployeeTest {

    private EmployeeRepositoryInterface employeeRepository;
    private EmployeeController controller;
    private EmployeeValidator employeeValidator;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeMock();
        controller         = new EmployeeController(employeeRepository);
        employeeValidator  = new EmployeeValidator();
    }

    @Test
    public void testRepositoryMock() {
        assertFalse(controller.getEmployeesList().isEmpty());
        assertEquals(6, controller.getEmployeesList().size());
    }

    @Test
    public void test1() {
        Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = null;
        assertTrue(employeeValidator.isValid(newEmployee));
        controller.modifyEmployee(oldEmployee,newEmployee);
        assertEquals(null, controller.getEmployeeByCnp(newEmployee));
    }


    @Test
    public void test2() {
        Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = new Employee("Florin","DDD", "1234567891255", DidacticFunction.TEACHER, "1400");
        controller.addEmployee(oldEmployee);
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeValidator.isValid(oldEmployee));
        controller.modifyEmployee(oldEmployee,newEmployee);
        assertEquals(null, controller.getEmployeeByCnp(newEmployee));
    }

    @Test
    public void test3() {
        Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
        Employee oldEmployee = new Employee("Florin","DDD", "1234567891234", DidacticFunction.TEACHER, "1400");
        controller.addEmployee(oldEmployee);
        assertTrue(employeeValidator.isValid(newEmployee));
        assertTrue(employeeValidator.isValid(oldEmployee));
        controller.modifyEmployee(oldEmployee,newEmployee);
      //  assertEquals("Baciu", controller.getEmployeeByCnp(newEmployee).getLastName());
    }




}
