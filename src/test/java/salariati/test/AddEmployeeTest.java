package salariati.test;

import static org.junit.Assert.*;
import salariati.model.Employee;

import org.junit.Before;
import org.junit.Test;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

public class AddEmployeeTest {

	private EmployeeRepositoryInterface employeeRepository;
	private EmployeeController controller;
	private EmployeeValidator employeeValidator;
	
	@Before
	public void setUp() {
		employeeRepository = new EmployeeMock();
		controller         = new EmployeeController(employeeRepository);
		employeeValidator  = new EmployeeValidator();
	}
	
	@Test
	public void testRepositoryMock() {
		assertFalse(controller.getEmployeesList().isEmpty());
		assertEquals(6, controller.getEmployeesList().size());
	}
	
	@Test
	public void test1() {
		Employee newEmployee = new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
		assertTrue(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(7, controller.getEmployeesList().size());
		assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

	@Test
	public void test2() {
		Employee newEmployee =new Employee("Florin","", "1234567891234", DidacticFunction.TEACHER, "1400");
		assertFalse(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(6, controller.getEmployeesList().size());
		assertFalse(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

	@Test
	public void test3() {
		Employee newEmployee =new Employee("Florin","Baciu", "", DidacticFunction.TEACHER, "1400");
		assertFalse(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(6, controller.getEmployeesList().size());
		assertFalse(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

	@Test
	public void test4() {
		Employee newEmployee =new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "");
		assertFalse(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(6, controller.getEmployeesList().size());
		assertFalse(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}


	@Test
	public void test5() {
		Employee newEmployee =new Employee("Florin","DD", "1234567891234", DidacticFunction.TEACHER, "1400");
		assertFalse(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(6, controller.getEmployeesList().size());
		assertFalse(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

	@Test
	public void test6() {
		Employee newEmployee =new Employee("Florin","D", "1234567891234", DidacticFunction.TEACHER, "1400");
		assertFalse(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(6, controller.getEmployeesList().size());
		assertFalse(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

	@Test
	public void test8() {
		Employee newEmployee =new Employee("Florin","Baciu", "1234567891234", DidacticFunction.TEACHER, "1");
		assertFalse(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(6, controller.getEmployeesList().size());
		assertFalse(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

	@Test
	public void test9() {
		Employee newEmployee =new Employee("Florin","jhsbdjsdkbvsbhjnhbv", "1234567891234", DidacticFunction.TEACHER, "1");
		assertFalse(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(6, controller.getEmployeesList().size());
		assertFalse(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}

	@Test
	public void test10() {
		Employee newEmployee =new Employee("ajhsbadifjksbdkvjbdsvjkbdsk","Baciu", "1234567891234", DidacticFunction.TEACHER, "1400");
		assertTrue(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(7, controller.getEmployeesList().size());
		assertTrue(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}


	@Test
	public void test7() {
		Employee newEmployee =new Employee("Forin","Baciu", "12345678912345", DidacticFunction.TEACHER, "1");
		assertFalse(employeeValidator.isValid(newEmployee));
		controller.addEmployee(newEmployee);
		assertEquals(6, controller.getEmployeesList().size());
		assertFalse(newEmployee.equals(controller.getEmployeesList().get(controller.getEmployeesList().size() - 1)));
	}



}
